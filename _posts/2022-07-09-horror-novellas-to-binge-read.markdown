---
layout: post
title:  "Horror Novellas To Binge Read"
date:   2022-07-01 06:32:14 -0500
categories:
tags: 
metadesc: "horror novellas, short books, quick reads, with teeth, below, go down hard, brian keene, hailey piper, stephen graham jones"
hp: "Here's some great horror novellas you can read in a day or two."
---
One of the best things about the indie horror fiction scene is the rise of the horror novella. Here are some shorter books that you can read in a day or two!


### <strong><em>Below</em> by Laurel Hightower</strong>

_1408_ in a cave with Mothman, kinda. It's got perfect short chapters, each ending in a setup for various horrors in the dark.

<a href="https://amzn.to/3bTCPGk" target="_blank">Buy <em>Below</em> on Amazon</a>

<p></p><p></p>
### <strong><em><a href="https://amzn.to/3bTqEsU" target="_new">Go Down Hard</a></em> by Ali Saey</strong>

Super readable with quick focused chapters that switch between two serial killers who live next door to each other.
<a href="https://amzn.to/3bTqEsU" target="_new"> Buy <em>Go Down Hard</em> on Amazon</a>

<p></p><p></p>
### <strong><em><a href="https://amzn.to/3P2xXgk" target="_new">With Teeth</a></em> by Brian Keene</strong>

Incredibly violent, feral vampires. 

<a href="https://amzn.to/3P2xXgk" target="_new">Buy <em>With Teeth</em> on Amazon</a>

<p></p><p></p>
### <strong><em><a href="https://amzn.to/3OOo1an" target="_new">The Worm and His Kings</a></em> by Hailey Piper</strong>

Cosmic horror in 1990s New York City sewers.

<a href="https://amzn.to/3OOo1an" target="_new">Buy <em>The Worm and His Kings</em> on Amazon</a>

<p></p><p></p>
### <strong><em><a href="https://amzn.to/3yTOez7" target="_new">Waif </a> </em> by Samantha Kolesnik</strong>

It’s got a clandestine plastic surgeon named Mince; cmon, that’s pretty good.

<a href="https://amzn.to/3yTOez7" target="_new">Buy <em>Waif</em> on Amazon</a>

<p></p><p></p>
### <strong><em><a href="https://amzn.to/3OOnk0L" target="_new">Night of the Mannequins</a></em> by Stephen Graham Jones</strong>

Nothing sells this book better than its incredible opening sentence:

> So Shanna got a new job at the movie theatre, we thought we’d play a fun prank on her, and now most of us are dead, and I’m really starting to feel kind of guilty about it all.

<a href="https://amzn.to/3OOnk0L" target="_new">Buy <em>Night of the Mannequins</em> on Amazon</a>


<iframe src="//rcm-na.amazon-adsystem.com/e/cm?o=1&p=12&l=ur1&category=audibleplus&banner=0MG2XKQ7PYPP84NBNFR2&f=ifr&lc=pf4&linkID=bf74159e39854736f5852d2892f2c8e8&t=deadreader-20&tracking_id=deadreader-20" width="300" height="250" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0" sandbox="allow-scripts allow-same-origin allow-popups allow-top-navigation-by-user-activation"></iframe>

