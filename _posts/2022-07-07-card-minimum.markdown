---
layout: post
title:  "Card Minimum"
date:   2022-07-07 06:32:14 -0500
categories:
tags: short-stories

metadesc: "horror short story, pandemic horror, covid, soda, murder, free short stories"
hp: "an original short story of pandemic horror."
---

<span class='meta'>written by Brian Sullivan</span>

<span class='meta'><em>I wrote this short story early in the Covid pandemic. I hope you enjoy!</em></span>


<hr> 
<br><br>
I had a 20-ounce bottle of Coke in one hand and my phone in the other.

I was in the deli looking at my brokerage account and waiting for my sandwich. I shuffled in place and adjusted my cloth mask. I breathed in through my teeth as Apple shaved off another 20 cents.  The stale air channeled through a cavity. The hole in my tooth screamed in open rebellion, mocking me: What are you gonna do about it? 

I inhaled the musty paperback scent trapped in my mask. I had to wash it. I had a dozen of these things, most of them fresher than the one I was wearing, but I usually just took the one closest to me on my way out the door. 

The big crowd in the deli made me nervous. A few months ago, I would have walked right out if I came in here and saw this gaggle of construction workers and tourists. But we were a year into the pandemic, and I was hungry. Fuck the tourists. Fuck the construction workers who wear their masks on their chin and touch every paper cup in the stack when preparing their coffee. Fuck making my own food.

I looked at the Coke in my hand with sour regret. I should have waited until my food was ready before grabbing it from the fridge. Judging by how many people were waiting for their orders, I was going to be here for a while. I thought about putting it back and then picking up a cold one right before I left.  That seemed a little uncouth in pandemic times, putting your dirty hands on something and walking off.  

I looked around the deli, trying to distract myself from constantly checking my phone. I watched an older man in a reflective vest inspect a bag of artisanal pita chips. He held it in front of him in both hands, gawking, probably at the price tag. He looked like he might start drooling. If he did, his face mask would catch it – it was scrunched up on the bottom of his chin. He filled his cheeks with air and made a fish face. He puffed, again I can only assume in defiance of the orange $4.95 sticker. 
He sniffled and his eyes fluttered. He let go of the bag with his left hand and sneezed into his open palm. He looked at his hand for a moment as if confused, wiped it on his dirty jeans, and then grabbed the bag again so he could continue staring at it incredulously. He turned the bag over and over, inspecting it. _It’s a fucking bag of chips,_ I thought. Buy it or don’t. He put the bag back on the shelf and picked up another. They were the same chips. The same flavor.

I walked over to the fridge wall, opened the door and put my soda on a bottom shelf by itself. I wasn’t going to stoop to that guy’s level, but it bothered me that I was being so cautious when other people were careless slobs. A yellow froth of paranoia and anger boiled in my stomach. I looked at my phone and discovered that I had lost another $300 watching this guy sneeze.

There was one guy behind the deli counter. He dashed back and forth from the grill to the counter. He flipped burgers, dipped cages of fries in oil, squirted condiments and mixed salads. Half a dozen incomplete orders were lined up on the counter. I spotted mine, roast beef on a Kaiser roll. I knew it was mine because I had asked for no mayo and the sandwich drying out on the counter was slathered in it. He put the top half of the roll on the sandwich, cut it in half deftly and wrapped it in foil.

"Roast Beef with Mayo!" he announced to the waiting customers and put the sandwich on top of the counter.  No one came forward.  I sighed and grabbed my awful sandwich.

I walked back to the drinks, my eyes on the bottom shelf where I had stashed my Coke. As I approached it, I saw a hand shoot out on the other side of the fridge wall. There was someone back there stocking drinks. He picked up my soda and moved it back to the top shelf with the others of its kind. So much for trying to be courteous. I opened the door, my sleeve wrapped over my hand, and picked out a Cherry Coke this time instead.

I got in line behind half a dozen people. There were markers on the floor six feet apart, and friendly notices instructing customers to keep their distance. I seemed to be the only one aware of the signage. I stood back abought eight feet from the Pita Chips Guy, who was in line in front of me. Then another construction worker walked up and casually filled the space between us. My mind flashed with images of chains and purple flesh.

"Excuse me," I said, but he didn’t turn around. I put my hand back in my coat sleeve and tapped him on the shoulder. He turned his head sideways so he could see over his shoulder.

"Yeah?" he said. He sounded like I was interrupting something.

"I was in line." I braced myself for an argument.

"Oh. Sorry." He moved from his spot and stood behind me. 

I could feel him breathing down my neck. It didn’t make sense – he was wearing a mask and there were at least a couple of feet between us, but I felt hot breath on the nape of my neck just the same. I thought I could even smell it. Stale beer and spit. I moved up a few inches, taking my chances being closer to Pita Chips Guy instead.

Pita Chips Guy was in front of the register putting his items down on the counter. He’d went with a snack size Doritos bag to go along with his Canada Dry instead of the pita chips. He dug through his wallet and flashed a credit card to the cashier.

"Eight dollar minimum," the cashier responded automatically.

"Eight dollars? That’s ridiculous," the man said. I agreed with him, but if he held up the line over this I was going to scream.

The cashier wasn’t budging. "Sorry, it’s policy. If you want to use the ATM, I can keep your stuff here-"

"Eight dollars?" the man said again.  He turned around with his arms open at his sides, looking for support from the crowd in a gesture that said Can you believe this shit?
That didn’t impress the cashier. He waved me over. 

"Okay, next?" 

I started walking over and pita chips guy turned back to the register. 

"Alright, wait. Let me find my cash."

He looked in his wallet, patted down his vest and his jeans, and produced a dollar and twenty cents worth of change. 

"How about I pay on my card and give you this to cover the minimum?"

"What? I’m sorry, we really can’t do that." The cashier motioned to me again and I put my soda and sandwich on the counter. 

"Hold on, hold on" the man said. "I’ve got the money."

What this man did next drove me to murder.

He bent down and untied one heavy work boot, taking his time. He took the boot off and I got a good view of an old foot in a dirty sock. His big toe was sticking out entirely through a rip in the fabric. The toenail was overgrown, the top edge of it a bold quarter-inch tusk of sickly green-yellow. It grew out of the nail’s serrated, cloudy surface, a black blood spot blooming in the nail bed underneath. He reached into the sock and pulled out a crumpled five. He put his boot back on, tying it only loosely. A loop of shoelace flopped onto the ground.

He put the five on the counter and flattened it with his palm. Then he used the same hand to pick up a Tomblerone bar. He looked at it for a moment and put it back.
"Do you want a bag?" the cashier asked.

"Yeah, sure," replied Chips/Foot man. He put his card and wallet back in his back pocket.

Then the cashier added twenty cents to his total and Foot man slammed his fist on the counter.

"I have to buy the bag? Forget it!"

The cashier subtracted the twenty cents and finally rang him up without further comment. 

Foot Man took two steps forward and stopped to hitch up his pants. I almost walked right into him. I looked down and saw his boot had become untied, and I had almost stepped on the shoelace.

I put my soda and sandwich on the counter and took out my credit card.

"That’ll be seven twenty-four," said the cashier. I sighed and bought a Tromblerone to hit the minimum.

Foot Man was slow. He had a head start but I still had to stand and wait behind him as he opened the door to leave. He started walking across the street to the construction site. My apartment was the other way. I followed him.

The construction site was for an upcoming luxury apartment skyscraper that towered over the twelve-story building where I lived in a studio apartment. When it was finished, the new building would completely obscure the great view I had of Manhattan. It was being built on a gigantic dirt lot that used to house a park and a library. 
The distance between the sidewalk and the back of the lot where the building construction started was a maze of scaffolding and temporary walkways walled with graffitied plywood.  

I followed Foot Man into this maze. I noted that there were fish-eye mirrors in the corners so he could see me behind him if he looked, but I didn’t spot any cameras. That was good.

Foot Man worked on opening his Doritos bag as he turned a corner, not looking at the mirror. I followed him into a long straight section. The plywood walls here were swollen with rain and there were trash-filled puddles on the concrete floor. We were alone.  I picked up my pace until I was close enough for him to feel my breath. He didn’t sense me. I met his pace and studied the rise and fall of his step. His right boot came down and splashed into a puddle. I stepped on his wet shoelace right as he swung his other leg forward.


He fell in a belly flop. His chin hit the concrete. His bag of Doritos flew into a puddle, looking right at home with the rest of the garbage. His other hand was still clutching the Canada Dry can, but then his grip loosened and the can rolled on the concrete, coming to a stop at his side.

I looked around. We were still alone, but someone might have heard him fall. I stepped in front of him and peered down so I could see his face. He looked straight forward, his jaw on the floor.  He was breathing in shallow gasps, sticky bubbles of blood forming at his lips and popping when he spit through them. There was a lot of blood and I wondered if he had bit down on his tongue, or maybe broke a rib or something. 

I kneeled down, pulling my sleeves over my hands, and ripped his mask off his chin. It only had a few spots of blood on it. I yanked his nose up and bottom lip down and pried his mouth open. He screamed. He lifted one arm and batted weakly at my side. When his arm hit the ground again, I lifted my knee and pinned it there. 

His tongue was still intact, but there was a deep cut where he bit down.  Using the mask, I pinched his tongue between my forefinger and thumb and pulled it. With my other hand I grabbed the can of Canada Dry, opened it, and twisted off the can tab. I poured the soda onto his tongue, staring into his eyes with a warm smile. The soda mixed with the blood and dribbled out of his mouth in a frothy pink spit. I held out the tab for him to see. His eyes were wet with pain and fear. I balanced the tab over the slit in his tongue and squeezed down, forcing it in deep. The unmistakable sounds of assault echoed in the damp hallway. 

We were still alone, but I had to shut him up. I stepped on the can and crushed it. I forced it into his mouth, feeling it scrape against and through broken teeth. He tried to squirm his arms out from under me. I lifted one leg and drove my knee into his face. He groaned and his arms fell slack.

I went to his feet and untied his loose shoelace. I used it to tie his hands behind his back, then I walked around to his ruined face. I placed his mask on him, fitting it over one ear and then the other. The can sticking out of his mouth bulged underneath the mask. The bottom of the mask was already pooling with blood. We were still alone.

I wanted him to know why this was happening to him. I moved to his legs and pulled off his untied boot. I used my sleeve to grab at his smelly socked foot and gripped the protruding toenail. I peeled it off like a scab. I walked back to his front and kneeled down. I displayed the toenail to him with another smile, then shook my fingers in a no-no gesture. He didn’t seem to understand, so I made him see. I pushed the toenail in his eye and adjusted it so it curved over this eyeball, pinching it. I pushed in and up, forcing the toenail to slide up under his eyelid. The jagged nail pushed out the skin and stuck out from the bottom like a tumor.

I took off his belt. I wrapped it around his neck and sat on his back. I pulled the belt tight and his head lifted off the ground. After a few seconds, I heard something – a conversation in the distance – and let go of the belt. His jaw hit the floor again.

I’d touched the belt with my bare hands, so I unraveled it from his neck and shoved it into my coat pocket. Then I picked up my sandwich and soda from the ground and walked away. 

I saw no one on my way out, and I didn’t hear the voices again. I walked out of the construction site and crossed the street. In my apartment building, I used the hand sanitizer pump while I waited for the elevator.

The elevator seemed to travel in slow motion. I looked down at my clothes. There were a few blood stains – a big one on my knee – but no one seemed to notice when I was walking back.

I walked into my apartment. I threw my sandwich in the trash can, sat down on my bed and drank my Cherry Coke.
<hr>
