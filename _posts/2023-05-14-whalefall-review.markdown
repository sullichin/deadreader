---
layout: post
title:  "Whalefall Review"
date:   2023-05-14 06:20:14 -0500
categories:
tags: 
book_title: "Whalefall"
book_author: "Daniel Kraus"
metadesc: "Whalefall by Daniel Kraus book review"
hp: "A psychedelic odyssey in the belly of a whale."
---

<em>Whalefall</em> is astonishing, a psychedelic odyssey in the belly of a whale. An escape room puzzle with clues from the past and from beyond the grave. Claustrophobic, scary and supposedly scientifically accurate: hell, I believed it. I paused a couple of times to look up some visual references, I'm no diver, and found myself in a rabbithole reading about giant abyssal sea creatures. Incredible stuff, but the book alone is enough to make you feel small. The book is visual on a grand & biblical scale, the ocean a primordial, alien world that holds all truth within. I read part of it while wading in the ocean, letting the book suffuse into my brain in the way really good stories can.

Jay's adventure is written in real-time present tense, with flashbacks (also present tense). Without spoiling anything, his father Mitt was an experienced diver -- who taught seventeen year old Jay everything he knows about diving -- and him and Jay had a complicated relationship before Mitt's death. Jay's experience here is about confronting his grief, finding himself and his place. My own father died when I was fifteen, I have older sisters and no brothers like Jay, and while my relationship with my own father was very different, this hit me, like Kraus describes a wave at one point, like a chest of drawers. I felt like I was in a mushroom trip exploring my own grief. 


Learn more about the author at <a target="_blank" href="https://www.danielkraus.com/">danielkraus.com</a>


