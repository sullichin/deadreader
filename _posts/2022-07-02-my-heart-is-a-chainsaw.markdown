---
layout: post
title:  "Slasher 101"
date:   2022-07-03 06:20:14 -0500
categories:
tags: 
book_title: "My Heart is a Chainsaw"
book_author: "Stephen Graham Jones"
metadesc: "My Heart is a Chainsaw, Stephen Graham Jones Book Review"
hp: "Stephen Graham Jones really likes slashers."
---

Stephen Graham Jones is one of the absolute best horror writers living today. He's probably one of the best fiction writers period, but I don't read enough not-horror to really make that claim. His casual-literary style is just unmatched for me. If you enjoyed _The Only Good Indians_ or _Mongrels_ just buy this. If you couldn't get on board with his style and run on sentences there, you probably won't like it here either. 

While I truly loved those books (and _Night of the Mannequins_, and _Mapping The Interior_...), with this one SGJ has created one of my favorite fictional characters in Jade, a horror mega fan who finds herself in the middle of a real life slasher (or so she thinks). Some may not like the constant references to slasher movies here, but I was totally on board and even watched some of them to better get in the mood. 


The book is a total love letter to the genre, Jade so self-aware of every possible trope that every possibility is brought up and examined, keeping you guessing and playing with expectations. Jade is more than her love of slasher movies though and I was very engaged spending 400 pages with her. 

When it's time for the gore, _My Heart is a Chainsaw_ lives up to its name with a healthy amount of gruesome kills.
