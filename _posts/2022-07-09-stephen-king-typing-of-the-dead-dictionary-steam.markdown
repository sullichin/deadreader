---
layout: post
title:  "Stephen King dictionary for Typing of The Dead: Overkill on Steam Workshop"
date:   2022-07-09 05:30:14 -0500
categories:
metadesc: "Stephen King, steam mod, game mod, typing of the dead, custom dictionary, steam workshop, the shining, the stand, duma key, revival, salems lot"
hp: "Check out my Stephen King dictionary for Typing of the Dead that includes over a thousand passages from 50+ King works!"
---

A while back I made STEPHEN KING: TYPE 19, a custom dictionary for the Steam game _Typing of the Dead: Overkill_. If you're unfamiliar, the game is the on-rails shooter _House of the Dead_, but as a typing game.

![Stephen King Type 19 Dictionary](https://pbs.twimg.com/media/FXP8c-MXwAIJ_MF?format=jpg&name=4096x4096)


I've been updating the dictionary file and I just released a new version with over 1,200 entries from 50+ Stephen King novels, novellas, and short stories. It's got tons of King staples (Chambray shirts, arc-sodium lights), and plenty of other fun things to type like:

* bully chickenshit fucker
* Dumbo's Big Jumbo
* not-quite-broken ass
* shroud of sandy spices

[Subscribe to Stephen King TYPE 19 on Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1953633076) to download! Then enable the dictionary in-game. Disable difficulty settings to allow the custom dictionary to work across all difficulties.

