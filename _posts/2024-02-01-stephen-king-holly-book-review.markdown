---
layout: post
title:  "The America in Stephen King's Holly Doesn't Exist"
date:   2024-02-22 06:20:14 -0500
categories:
tags: 
book_title: "Holly"
book_author: "Stephen King"
metadesc: "Holly by Stephen King book review"
hp: "Holly is fun when it's a procedural detective novel, but loses momentum often."
---
Holly Gibney, Stephen King's favorite character since _The Dark Tower_'s Roland, is back again in her second POV novel, one that's so much about Holly, it's called HOLLY. Off the heels of _The Outsider_ and the novella _If It Bleeds_ where Holly uncovers the mystery of supernatural, shapeshifting foes, _Holly_ is about a much more grounded concept: elderly cannibals.

The characters feel real, but the America in _Holly_, where a teenager wishes he had $3 for a burger and kids play arcade games in an ice cream shop in 2018, does not exist. The small worldbuilding details that King is often so good at didn't really land for me here, from skateboarding kids who don't talk like modern kids, to an over abundance of COVID-focused dialogue when the plot moves forward to 2020. It's these details, not the presence of the old man and woman who eat people to stay young, that frequently took me out of the story.

But the biggest pace killer in _Holly_ are most of the chapters involving Barbara. She's a pretty good character in the other Bill Hodges-universe books, but unfortunately she's not given anything interesting to do here for the majority of the story. She's busy writing poetry and connecting with her new poetry mentor, the elderly woman in the book's main cast who _isn't_ a cannibal.

When Holly is doing detective work, though, the book flies and showcases King's prowess for drip-feeding information to the reader and characters in a satisfying manner. While all the COVID talk did get a little overbearing at points -- I was simply sick of hearing about it in real life I think -- it makes sense for Holly as a character to be so cautious and vocal about it. The villians, Emily and Rodney Harris (a nod to Thomas Harris for sure) are both evil and pathetic in that way that King does so well. Using their age to both lure their victims (won't someone help the hurt old lady!) and avoid suspicion, Emily and Rodney go about their business of eating brain salves so they can stay healthy. They are cunning and desperate and completely irredemable. 

The conclusion of _Holly_ went in a direction I wasn't quite expecting and I loved it. I came out of the story liking Holly's character a lot more. Good thing Stephen King doesn't seem like he's going to give her up any time soon.