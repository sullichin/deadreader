---
layout: post
title:  "Resident Evil Village: Winter's Expansion Review"
date:   2022-10-28 05:30:14 -0500
categories:
metadesc: "Resident Evil Village: Winter's Expansion (Shadows of Rose) video game review"
hp: "<em>Shadows of Rose</em> is a worthy coda to the Ethan Winters saga."
---

Remember in <em>Resident Evil Village</em> when you had to collect four jars containing your infant daughter's body parts to beat the game? Who can forget?

Well that daughter, Rosemary Winters, is all grown up now. She's sixteen, and she just wants to be <em>normal</em>, which is difficult because her body is comprised of a fungus that replicates human DNA. She sweats weird white stuff and her classmates make fun of her for having "weird powers," although how these powers -- the ability to control this fungus, the Mold which is first introduced in <em>Resident Evil 7</em> -- actually manifested in her daily life is unclear. 

Thankfully there's a Purifying Crystal that can unlink Rose from the fungus, and to secure it she only needs to make metaphysical contact with a remaining fragment of the God fungus thought totally destroyed in <em>Resident Evil Village</em>, and obtain it deep within the Realm of Consciousness.

Pretty straightforward, right? Like <em>Village</em>, the <em>Shadows of Rose</em> campaign takes a ridiculous and convoluted story and plays it nearly totally straight. That's the beauty of <em>Resident Evil</em>: when it finds that balance of horror and action-movie camp. 

The story sends Rose through multiple locations from the original game including Castle Dimtrescu. In fact there's almost nothing new here at all in terms of environments, but I love <em>RE8</em> so I was content to revisit these places. The game is as beautiful as ever and I enjoyed the new third-person perspective. Kind of like the B campaigns in <em>Resident Evil 2</em>, the castle is remixed with new routes and puzzles. The Duke returns in remixed form and his VA Aaron LaPlante gives a wonderful performance. Less interesting is the fact that there's globs of reddish-black mold everywhere. It's a pretty standard videogame trope -- here's an area you've been before but now it's corrupted with some weird goopy stuff. 

Rose is not Ethan Winters though, and the gameplay is quite different than the main game. She's not nearly as well-armed and there's more focus on avoidance than combat. Her powers amount to being able to slow down or temporarily disperse enemies. While effective, Rose never really feels <em>powerful</em>, which is a good choice. Survival horror works best when the protagonist is actually vulnerable. And although the player will recognize pretty much every room and character from Ethan's campaign, it's all new and frightening to Rose.
<div style='float: left; padding: 4px; margin-right: 15px'>
<iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=deadreader02-20&language=en_US&marketplace=amazon&region=US&placement=B08TYCWK3C&asins=B08TYCWK3C&linkId=700fa42455f41328d0200e37ccebf684&show_border=true&link_opens_in_new_window=true"></iframe>
</div>
Her limited ability to dispatch enemies kept me at a relatively slow pace while exploring. The game is fairly linear and perhaps has too much sign-posting, even with tutorial messages off (speaking of tutorial messages, you'll want those on for the final boss). There's an over-abundance of glowing guidance text and yellow "progress paint" that seem unecessary. 

After the castle Rose is transported to House Beneviento, home to the infamous spooky baby from Ethan's campaign. Just like the main game, Rose loses her weaponry. The DLC peaks here and has some scary moments that rival the original House Beneviento segment. There's an interesting mechanic involving moving mannequins that filled me with anxiety in the best way. 

Rose visits a couple more familiar locations to wrap things up, and the story bits are as ridiculous as you'd expect. My playtime clocked at 2 hours 40 minutes on hardcore, but that didn't count restarts from a handful of frustrating checkpoints. It's a pretty decent length for a DLC campaign, although I'm not sure I see myself revisiting it, unlike the main game which I've played through over a dozen times. There's no unlockables like new weapons or infinite ammo to mix things up, either, which is a bit of a bummer.

Speaking of the main game, the DLC also adds the option to play that in third-person as well (although you can't play <em>Shadows of Rose</em> in first-person). Capcom's insistence on not showing Ethan's face extends to the control scheme and camera of the third-person implementation. Hilariously, you can't pan the camera to face him, as it will aggressively turn itself around. For whatever reason Rose does not have this limitation. Coupled with the oddity that diagonal movement input (W+A on keyboard) toggles sprint off, the result is that playing the main game in third-person doesn't feel quite like it should. Going back to <em>RE2</em> which has wonderful keyboard/mouse controls to confirm, and yeah, controlling Ethan just feels a little off in third-person. Still, I'm happy for the option and I'm sure I'll play through the game at least a couple more times with the new perspective.

The last bit of the DLC is <em>Mercenaries Additional Orders</em>, which adds new characters and stages to Mercenaries. It's a fun addition and I'm hoping I can finally get those damn S ranks with the stronger characters.

Overall I think <em>The Winter's Expansion</em> is pretty good, if a bit underwhelming. If you love the series like I do it's a must-play, although it rarely reaches the heights of the main campaign.


[Search Stephen King deals on Amazon](https://amzn.to/3yxMHNw) (ad)

