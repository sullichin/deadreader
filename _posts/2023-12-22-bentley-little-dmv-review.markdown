---
layout: post
title:  "Bentley Little is ridiculous"
date:   2023-12-22 06:20:14 -0500
categories:
tags: 
book_title: "DMV"
book_author: "Bentley Little"
metadesc: "DMV by Bentley Little book review"
hp: "DMV is a lukewarm serving of Bentley Little's signature dish."
---
The only thing new about this Bentley Little novel, in which the titular Department of Motor Vehicles is really a secret, ancient evil organization, is that there is no "THE" in the title. Call this "THE DMV" and it'd fit right at home with all the other Bentley Little books on your shelf: _The Haunted_, _The Academy_, _The Store_, _The Collection_, _The Town_, _The Mailman_...

What these books all have in common is what Little does best, which is taking a mundane institution and making it evil. That department store, that charter school? They aren't what they seem.

What better institution to send up than the DMV? The World's Most Boring Place to Wait In is the perfect playground for Little's style of satire where the otherworldly meets the mundane. Characters battle paperwork, long lines, driving tests, license renewal procedures, and...bees? Yeah, this one gets weird. The main theme is that the DMV is actually _incredibly_ competent has far more power and influence than they should. The DMV in the book's everyday-generic American town is kind of doing a _Needful Things_ style manipulation of turning the townspeople against each other.

In typial Little fashion the characters seem like sketches of real people with few unique traits. The tech jargon from "IT Guy" or "web designer" characters is hilariously wrong. But, it doesn't really matter. I think Little's books are probably best enjoyed zoning out on the beach without thinking too much, and that is not a criticism. _DMV_ is just a fun book. You want to read them not for the characters, but to witness how these characters are increasingly manipulated and affected by the "prime evil" at the heart of the story; there's usually one or two characters who knows what's really going on, and one that's already been brainwashed by the evil (e.g. becomes an employee of _THE_ Store or _THE_ DMV) so we can get that inside perspective. 

If you've read one Bentley Little Book, you've read five others, and _DMV_ doesn't stray too far from his formula, which of course he's very good at by this point. The story eventually becomes wholly unbelievable, though, even considering the suspension of belief required to buy the idea of a DMV that can legally kidnap you. 