---
layout: post
title:  "The Elevator"
date:   2024-03-15 06:32:14 -0500
categories:
tags: short-stories

metadesc: "horror short story, free short story, elevator horror, stuck in elevator"
hp: "a horror short story about an elevator that won't stop."
---

<span class='meta'>written by Brian Sullivan</span>

<span class='meta'><em>This is a short story I wrote for a prompt-based exercise: You enter an elevator, but instead of stopping at your floor, it keeps going down...</em></span>


<hr> 
<br><br>
I’ve delivered to this building before but never to the penthouse. I didn’t even know this building had a penthouse but there it was right on the digital elevator panel: not floor 30 next to the other numbers but PH, its icon a shiny brushed metal circle proudly centered above these lesser floors. I tapped it and then someone cleared their throat loudly behind me.

“Can I help you, sir?”

I turned around and there was Robert, a doorman I’ve interacted with several times delivering to North Tower, the biggest building in the city. 

“Oh hey, Robert. Looks like you can’t get rid of me, huh?” I said. I smiled and lifted the McDonalds bag I was carrying in front of me. “I’m surprised people living in a penthouse don’t have more expensive taste.”

Robert, usually so friendly and talkative, raised one eyebrow and stared me down. He didn’t seem to recognize me. He offered no response.
“I’ve uh, got a delivery for the penthouse. Top floor. Name James Blinkin?”

Robert’s eyes didn’t meet mine, seemed to still stare through me. “Please check in at the front desk, sir.”

Then his eyes did flick up to mine, and for an instant they were wide, full of fear. Or was I imagining things?

“Follow me, sir,” he said in that same dull voice so unlike the animated Robert I thought I knew. Before I could respond Robert was already turning out of the elevator banks and headed to the front desk.

“What is your name, sir?”

Now I knew for sure Robert didn’t know who I was. Honestly, it shouldn’t have bothered me. This guy probably sees a hundred delivery drivers a day and probably makes the same small talk with all of them. But this was so unlike my last interaction with Robert just three days ago. I had walked in with a pizza box and Robert had just about broken out into song about it, greeting me by name.

“Michael,” I said, flustered.

Robert picked up the desk phone and held it to his ear. “Good evening. This is Robert at the front desk. A man here with some Big Macs for you. Should I send him up?”

Robert smiled at whatever the response was on the other end. “Of course, Mr Blinkin. Of course.” Then, with one hand over the phone, a quick dart in my direction. “Elevator six.”

I walked back over to the bank of elevators. The massive steel doors of elevator six slid open at my approach and settled with a squeak. From the outside, it looked like the other five elevators, but inside the textured metal walls were draped with a paint-stained drop cloth. A digital readout on an archaic panel of switches read “Ph” as best as LCD line segments could convey. This was the cargo elevator. 

I walked into elevator six and I am still in elevator six.

My first indication that something was wrong wasn’t Robert’s unusual rudeness, or his vaguely unsettling phone conversation, or the fact that I was ushered into the cargo elevator for the first time when all of the other elevators appeared to be free in an empty lobby. 

I realized something was wrong when the elevator started going down.

I said “fuck,” filled my cheeks with air and blew out. I guessed I’d have to go to the basement and hope I could call another elevator from down there. Probably have to find a staff member to buzz me up all over again. Fuck.

I looked at the McDonalds bag I was holding. The bottom was dark with grease. I was betting anyone who lived in a penthouse in this building wouldn’t take very kindly to his shitty delivery meal being late. Honestly, the food was probably garbage before I even left the restaurant; it’d just been sitting there on the counter. The smell of french fries was cloying, overbearing. The smell of McDonalds when you don’t want McDonalds is, as it turns out, fucking nauseating.

Soon enough the wall panel read “B1” and the elevator settled. A beep, a pause. The doors poised to open. A brief feeling of weightlessness, and then I was going down again. B2; no stop this time. There was only one floor left, I thought: a final stop at the massive underground parking garage. I had parked there once and knew there were stairs back up to the street. But the elevator kept going.

I’d like to say that things felt different at this point, that I felt transported to a new dimension or something, but the truth was I just felt like a doofus who was about to lose his shitty $3 tip. Maybe — surely — there was an unlisted B4 I didn’t know about. What else was even possible?
A panic that I am now nostalgic for began to set in. Things were so much better then. I was so close to normality, had only just been thrown off a ledge into the unknown. The real world still existed. In this new world, where I still sit here now slouched in the corner, there was only down, down, down.

It would take a little longer for my helplessness to fully settle in, though. How many “floors” did I go down before I realized that this might somehow never stop? It was impossible to say; the wall panel still read B3 and there were no pauses in the car’s descent.

I took a deep breath and looked at the panel of switches on the wall for an intercom button, an emergency stop, something. The 4x4 grid of unlabeled blue switches looked more like a breaker box than elevator controls.

“I would be careful with that if I were you.”A calm male voice from above and behind me. I flinched, then turned to a dome camera on the ceiling.

“Hello? I need help! Can you hear me?” I said, and uselessly waved my arms in front of me like a man on a deserted island flagging down a boat. Whoever this was could obviously see me.

“Yes, I can hear you perfectly.” the voice said cheerfully. “And I am here to help. What can I help you with?”

“I need to get off this elevator!” I yelled. I thought that much was clear. I tried to gain some semblance of composure. “I’m trying to go up, to the penthouse.”

“But you’re going down,” the voice said. Was this guy trying to get a rise out of me?

“Yes,” I said to the camera, to the judging red blinking light.  “How do I stop this thing?”

“You don’t. I can stop it from here, though. But only if you qualify.”

“Is this a fucking joke? Who are you?” I was furious, an enviable quality for someone now so resigned.

“Let’s clear up a few things,” the voice said. “I’m James Blinkin, you’re… Michael something, or was it Miguel? I forgot what the app said. And that’s my bag of McDonalds, and Robert was wrong even if he was joking. I would never order a Big Mac. In that bag are two cheeseburgers, a ten piece nugget, a large coke, and a large fry. Or at least, that’s what should be in the bag. If it’s not, I might not be able to help you at all.”

“Are you kidding me?” I said, again able to respond with anything but stunned incredulity. 

“No, and this is no joke. It’s more like a game,” James said.

“Are you the guy from Saw or something? What the fuck!”

The speaker crackled with laughter. “Interesting that’s what came to mind. You might be onto something. Open the bag.”

I looked at the paper McDonald bag I was now gripping tightly. It was sealed at the top, but I could tell already there was no drink in there. There was definitely no cardboard cup holder at the bottom. I ripped the sticker off and peered into the bag. Two cheeseburgers, large fry. No soda, no nuggets.

“There’s no soda or chicken nuggets. Look, it’s not my fault. They just hand me a bag,” I said, pleading.

“They just hand me a bag,” James repeated mockingly. “Pathetic. Well, I was going to tell you what switch to press so you can come on up but now what’s the point? My lunch is ruined.”

“Please, I’m begging you, let me off this god damn elevator,” I said.

“No,” James said, and then blinking light under the camera turned off completely. 

“James?” I asked. “Hello?” 

Silence. Nothing but the thrumming hum of the steadily descending elevator.

I wouldn’t hear James again. This was maddening itself in the minutes, hours, days that followed. I was being watched, or was I? Now I could see no camera on the ceiling. It had somehow disappeared. When did that happen? Was James a false memory that I made up to kill time in this still-descending hell car? Did he appear in my mind when I started writing this account on my phone, fingers greasy from french fries (McDonalds didn’t provide any napkins, either)?

My phone told me that time was passing, but without any service the world might as well have been frozen. 

I had lasted maybe five minutes after my final encounter with James before I tried my luck with the panel of switches. They were all identical, unlabeled. I tried the first one in the top left corner. Nothing. I went in a row flipping them all and flipping them back. No poison gas scored by maniacal laughter. Nothing.

Two hours later and dozens of useless switch order combinations later, I started getting hungry. I was as mad as James was that there was no soda in the bag. But I was smart enough, or dumb enough, to wait before eating anything. After another excruciating two hours of pacing back and forth and holding in my piss I decided to turn off my phone, still at 50%, to conserve battery. 

When I turned it back on, it was three hours later and I was one burger short, there was a puddle of my piss in the corner, and my fingers hurt from trying unsuccessfully to pry open the heavy doors of the gliding car. How long could I make this shitty food last? Have you ever drank a ketchup packet?

I must have fallen asleep. I am still on the elevator. I am still here. I am so thirsty. The control panel is gone. The door is gone. All the walls are the same. My throat hurts from screaming. My food is gone. I’m going back to sleep.

The lights are off now.

There’s something in here with me.

