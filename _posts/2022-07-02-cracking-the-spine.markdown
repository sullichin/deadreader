---
layout: post
title:  "Cracking The Spine"
date:   2022-07-02 05:30:14 -0500
categories:
metadesc: "Dead Reader, Horror literature, horror books, scary books, book reviews, book recommendations"
hp: "This website exists now."
---
Hi! I read and collect horror books, and I like talking about them, so I made this. I've been keeping track of the books I've read this year on [Twitter](https://twitter.com/readerofhorror/status/1477510050876952576) but I wanted a spot for longer discussion. The horror scene is thriving, especially indie horror, and there's just so many incredible authors now. I would love if people could eventually use this as a resource to decide what book to read next. I also enjoy pretending it's the early 2000s and people still make personal websites.

I try to be thoughtful of the hard work it takes to write, let alone publish a book. If i'm critiquing a book here, I probably still really enjoyed it. I have a couple ideas in mind, but I'm still not really sure what type of stuff will be on here. It's _about_ reading and writing horror literature, though. My memory isn't so hot, and I read a lot of books, and sometimes I wish I had written my thoughts down. So maybe I'll start there.

