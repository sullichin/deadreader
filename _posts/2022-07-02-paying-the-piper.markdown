---
layout: post
title:  "Paying The Piper"
date:   2022-07-03 06:32:14 -0500
categories:
tags: 
book_title: "Children of Chicago"
book_author: "Cynthia Pelayo"
metadesc: "Children of Chicago, by Cynthia Pelayo book review, indie horror, book summary, kindle, pied piper, indie horror novels, true crime, crime novel, historical fiction, fairy tales, stephen king"
hp: "If you read the first couple of pages, you might think it's a police procedural. And it is? But the thing is, there's also this mythic creature from a fairy tale who's killing children."
---
I just finished reading [_Children of Chicago_](https://amzn.to/3AqSJlE) by Cynthia Pelayo, which hey, you can get from that Amazon affiliate link right there, or check out on her [website](http://www.cinapelayo.com/books/). From the Goodreads description:

> <em>... horrifying retelling of the Pied Piper fairytale set in present-day Chicago... When Detective Lauren Medina sees the calling card at a murder scene in Chicago's Humboldt Park neighborhood, she knows the Pied Piper has returned. </em>

Now, this is a Bram Stoker-nominated, International Latino Book Award Winning smash hit. I'm late to the party but glad I finally got around to this and understand what makes this book so exciting.

If you read the first couple of pages, you might think it's a police procedural. And it is? But the thing is, there's also this mythic creature from a fairy tale who's killing children. 

Lauren Medina, homicide detective, has one trait you might expect for such a protagonist -- she's married to her job, and put her real marriage on hold because of it. But defying horror tropes, she's both The Cop and the Person Who Believes. She has a personal connection to the case, one which centers the story and allows it to dip into the supernatural almost immediately. A teenager is found murdered in the same lagoon where the body of Lauren's sister was discovered years earlier. A recurrance of graffitti -- PIED PIPER -- tells her that the same killer is back.

Lauren is guilt-driven and this guides most of her actions as she works relentelessly to try to stop more children from being killed. She is uncomfortably unwilling to allow herself moments of happiness, her more selfish moments not totally indulgent (she likes fancy coffee, but she drinks a lot of it because she's always working). Her knowing of the supernatural elements at play means she can't always be honest in the typical police interrogative settings, so she has to get creative in obtaining information about the Pied Piper. This dishonesty feeds into her guilt when a student calls her bluff about the real reason she wanted to help tutor him. Her agonizing guilt is explained further the more we learn about Lauren's encounter with The Pied Piper as a child.

It's sometimes hard for me to talk about a story without reducing it to comparisons to other fiction, but I think I also just like seeing similarities in things that I enjoy. Pelayo's Chicago (which I can only humbly assume by the intricate attention to detail to landmarks is _the_ Chicago) is a place where death and suffering has always thrived, much like Stephen King's Derry. Here, not every tragedy is neccessarily ascribed to the story's boogeyman, as is more presumed in _IT_, and instead of a small town ignoring its historical violence through a sort of drugged ignorance, there's a city whose populace sees violence as an inevitability. They still try to ignore it, but it's hard when it's never ending. Citizens protest an ongoing memorial for the murdered dead, not wanting it in their neighborhood, and a new marker is ready for the ground by the time they're done screaming about the last one. There are parents who move their families to new neighborhoods when gunshots sound too close too home, who find the hassle only slightly more reliable than running from a bullet. There are kids who want to grow up and escape financially or geographically, because they don't see a good future in the world around them.

Forests in fairy tales, the reader is told, are dark places of mystery. They're compared to the many parks in Chicago, bright beautiful places that Lauren loves and where she feels closest to her father, and also where children are sometimes found dead. _Children of Chicago_ tells you early on that fairy tales are real, before telling you, _no, really._

The prose is serious and at times falls into the dry comforts of a police procedural, and moments where the plot takes a pause for exhaustive asides about Chicago history. Considering the connection Lauren feels to her job and to the city itself, the often straightforward tone and abundance of detail feels justified. It's all the more effective when fantastical elements are introduced in such a grimly realistic setting. The Pied Piper is a fun supernatural boogeyman, and his machinations would feel at home in a gory slasher story. The most heightened violence in the book isn't necessarily the most affecting; a stabbing scene early on hits pretty hard compared to some more unbelievable deaths via supernatural means, but that might be intentional. Those deaths are just fun to read because the monster came out of the shadows. It's almost a relief.


