---
layout: page
title: Horror Book Recommendations
permalink: /scary-book-recommendations/
metadesc: "Horror book recommendations, scary book recommendations, best indie horror books"
---


# A big list of great horror books

| Book Title   | Author | Type | Tag | Tag |
|----------|-------|-------------|-------|--------|
| [11/22/63](https://amzn.to/3RfXHHR) | Stephen King | novel | time travel | science fiction |
| [And The Devil Cried](https://amzn.to/3P4gB2v) | Kristopher Triana | novel | extreme horror | splatterpunk |
| [Bag of Bones](https://amzn.to/3NLXjhw) | Stephen King | novel | ghosts | literary |
| [Bath Haus](https://amzn.to/3bOH9Xh) | PJ Vernon | novel | crime | thriller |
| [Below](https://amzn.to/3unYOf2) | Laurel Hightower | novella | creature feature | psychological horror |
| [Benny Rose, the Cannibal King](https://amzn.to/3yHZOgo) | Hailey Piper | novella | creature feature | folklore |
| [Bone White](https://amzn.to/3ONUQED) | Ronald Malfi | novel | small town | supernatural |
| [Booth](https://amzn.to/3usN3nn) | Jason Pellegrini | novel | time travel | science fiction |
| [Brother](https://amzn.to/3yLgcgo) | Ania Ahlborn | novel | family horror | cannibalism |
| [Carrie](https://amzn.to/3OJUGOD) | Stephen King | novel | psychic powers | epistolary  |
| [Children of Chicago](https://amzn.to/3nGaUMH) | Cynthia Pelayo | novel | crime | folklore |
| [Crossroads](https://amzn.to/3bGnE32) | Laurel Hightower | novella | family horror | psychological horror |
| [The Cuck](https://amzn.to/3yIU1XT) | Aron Beauregard | novella | extreme horror | splatterpunk |
| [The Dead Zone](https://amzn.to/3OSeQGg) | Stephen King | novel | psychic powers | literary |
| [Dear Laura](https://amzn.to/3RgRLyC) | Gemma Amor | novella | stalker | mystery |
| [Desperation](https://amzn.to/3nIEMIq) | Stephen King | novel | survival horror | supernatural horror |
| [The Devil Crept In](https://amzn.to/3P8BgCP) | Ania Ahlborn | novel | posession | demons |
| The Darkest Lullaby | Jonathan Janz | novel | posession | family horror
| [Different Seasons](https://amzn.to/3ONGrIp) | Stephen King | collection | multiple genres | novellas |
| [Dolores Claiborne](https://amzn.to/3Ik6UuY) | Stephen King | novel |  family horror | domestic horror |
| [Dracula](https://amzn.to/3uqC6Tp) | Bram Stoker | novel | vampires | epistolary |
| [Everything's Eventual: 14 Dark Tales](https://amzn.to/3bLAZXT) | Stephen King | collection | multiple genres | short stories |
| [The Exorcist](https://amzn.to/3NJkBo7) | William Peter Blatty | novel | posession | religious horror |
| [Fear](https://amzn.to/3RpwiDJ) | Ronald Kelly | novel | small town | creature feature |
| [The Final Girl Support Group](https://amzn.to/3yhWYx9) | Grady Hendrix | novel | slasher | meta horror |
| [The Fireman](https://amzn.to/3AnwKMy) | Joe Hill | novel | psychic powers | postapocalyptic | 
| [Firestarter](https://amzn.to/3IgRszu) | Stephen King | novel | psychic powers | dystopian |
| [Full Brutal](https://amzn.to/3aeHl1y) | Kristopher Triana | novel | extreme horror  | splatterpunk |
| [Ghost Story](https://amzn.to/3yJCaA7) | Peter Straub | novel | ghosts | literary |
| [Gilchrist](https://amzn.to/3bSOYLG) | Christian Galacar | novel | small town | cosmic horror |
| [The Girl Next Door](https://amzn.to/3P5LzaF) | Jack Ketchum | novel | extreme horror | true crime  |
| [Gloria](https://amzn.to/3ylrtCA) | Bentley Little | novel | multiple realities | family horror |
| [Go Down Hard](https://amzn.to/3NJBLSK) | Ali Seay | novella | crime | splatterpunk |
| [Gone to See the River Man](https://amzn.to/3yJcNyx) | Krostopher Triana | novella | extreme horror  | splatterpunk |
| [Hannibal](https://amzn.to/3RbgEvm) | Thomas Harris | novel | procedural | crime |
| [The Haunted](https://amzn.to/3utjQZz) | Bentley Little | novel | haunted house | meta horror |
| [The Haunting of Hill House](https://amzn.to/3yG7XSD) | Shirley Jackson | novel | haunted house | psychological horror |
| [A Head Full of Ghosts](https://amzn.to/3P1LCEo) | Paul Tremblay | novel | posession | meta horror |
| [Hearts Strange and Dreadful](https://amzn.to/3yJ48Mi) | Tim McGregor | novel | historical fiction | dystopian |
| [I Am Legend](https://amzn.to/3agIshn) | Richard Matheson | novel | vampires | postapocalyptic |
| [Insomnia](https://amzn.to/3bT15bq) | Stephen King | novel | multiple realities | dark tower |
| [The Institute](https://amzn.to/3OYLkhK) | Stephen King | novel | psychic powers | science fiction |
| [It](https://amzn.to/3unp356) | Stephen King | novel | small town | coming of age |
| [Itsy Bitsy](https://amzn.to/3IkwRdS) | John Lindqvist Ajvide | collection | creature feature | short stories |
| [The Jigsaw Man](https://amzn.to/3ybuMfu) | Nadine Matheson | novel | procedural | crime |
| [The Lake is Life](https://amzn.to/3ylPk56) | Richard Chizmar | novella | ghosts | supernatural horror |
| [The Last House on Needless Street](https://amzn.to/3AphIG5) | Catriona Ward | novel | crime | psychological horror |
| [Later](https://amzn.to/3bTV8Lx) | Stephen King | novella | crime | ghosts |
| [Legion](https://amzn.to/3OO33se) | William Peter Blatty | novel |  posession | crime |
| [Let the Right One In](https://amzn.to/3afBpW6) | John Lindqvist Ajvide | novel | vampires | coming of age |
| [Lisey's Story](https://amzn.to/3ahlhmZ) | Stephen King | novel | ghosts | literary |
| [The Long Walk](https://amzn.to/3yLOgsM) | Richard Bachman | novella | dystopian | science fiction |
| [The Lottery](https://amzn.to/3nCPSyB) | Shirley Jackson | short story | dystopian  | literary |
| [Manhunt](https://amzn.to/3yIyEGc) | Gretchen Felker-Martin | novel | dystopian | postapocalyptic |
| [Mapping the Interior](https://amzn.to/3NESere) | Stephen Graham Jones | novella | family horror | literary |
| [Mirror in the Attic](https://amzn.to/3bROs0y) | Jennifer Bernardini | novel | crime | thriller |
| [Misery](https://amzn.to/3Igpzrc) | Stephen King | novel | survival horror |  psychological horror |
| [Mongrels](https://amzn.to/3afBJUO) | Stephen Graham Jones | novel | werewolves | literary |
| [My Best Friend's Exorcism](https://amzn.to/3Aund6h) | Grady Hendrix | novel | posession | meta horror |
| [My Heart is a Chainsaw](https://amzn.to/3ycc1IE) | Stephen Graham Jones | novel | slasher | meta horror |
| [Needful Things](https://amzn.to/3AtCzrB) | Stephen King | novel | small town | supernatural horror |
| [Night of the Mannequines](https://amzn.to/3nEujO0) | Stephen Graham Jones | novella | comedy | literary |
| [Night Shift](https://amzn.to/3P4Vmxx) | Stephen King | collection | multiple genres | short stories |
| [The Night Stockers](https://amzn.to/3P4VEED) | Kristopher Triana and Ryan Harding | novel | comedy | splatterpunk |
| Nightmare Yearnings | Eric Raglin | collection | multiple genres | short stories |
| NOS4A2 | Joe Hill | novel | vampires | coming of age |
| The Only Good Indians | Stephen Graham Jones | novel | folklore | psychological horror |
| Pet Sematary | Stephen King | novel | family horror | supernatural horror |
| The Posession of Natalie Glasgow | Hailey Piper | novella | posession | supernatural horror |
| The Pretty Ones | Ania Ahlborn | novella | slasher | crime |
| Queen of Teeth | Hailey Piper | novel | body horror | dystopian |
| Razorblade Tears | S.A. Cosby | novel | crime | mystery |
| The Razorblades in my Head | Donnie Goodman | collection | multiple genres | splatterpunk |
| Red Dragon | Thomas Harris | novel | procedural | crime |
| The Regulators | Richard Bachman | novel | survival horror | supernatural horror |
| [Revival](https://amzn.to/3yGya3t) | Stephen King | novel | supernatural horror | coming of age |
| The Road | Cormac McCarthy | novel | postapocalyptic | literary |
| Rose Madder | Stephen King | novel | supernatural horror | domestic horror |
| Rosemary's Baby | Ira Levin | novel | supernatural horror | domestic horror |
| The Ruin Season | Kristopher Triana | novel | literary | coming of age |
| Sacrilege | Barbara Avon | novella | psychological horror  | family horror  |
| Salem's Lot | Stephen King | novel | vampires | small town |
| Seed | Ania Ahlborn | novel | posession | family horror |
| [The Shining](https://amzn.to/3ABsjO2) | Stephen King | novel | ghosts | family horror |
| [The Shuddering](https://amzn.to/3P7pHM8) | Ania Ahlborn | novel | survival horror | creature feature |
| The Silence of the Lambs | Thomas Harris | novel | procedural | crime |
| Skeleton Crew | Stephen King | collection | multiple genres | short stories |
| The Slob | Aron Beauregard | novella | extreme horror | splatterpunk |
| Snow | Ronald Malfi | novel | survival horror | creature feature |
| Son of the Slob | Aron Beauregard | novella | extreme horror | splatterpunk |
| [The Stand](https://amzn.to/3nEmHLo) | Stephen King | novel | postapocalyptic | supernatural horror |
| [Sundial](https://amzn.to/3P28R16) | Catriona Ward | novel | psychological horror | family horror |
| [Survivor Song](https://amzn.to/3Aq0S9V) | Paul Tremblay | novel | postapocalyptic | survival horror |
| [Tales from Greystone Bay](https://amzn.to/3P5aevT) | Robert McCammon | collection | ghosts | short stories |
| [The Talisman](https://amzn.to/3Pbbi1z) | Stephen King and Peter Straub | novel | fantasy horror | coming of age |
| [They All Died Screaming](https://amzn.to/3NLPMPB) | Kristopher Triana | novella | postapocalyptic | splatterpunk |
| [Things Have Gotten Worse Since We Last Spoke](https://amzn.to/3ym0sPp) | Eric LaRocca | novella | epistolary | psychological horror |
| The Thirteenth Koyote | Kristopher Triana | novel | werewolves | western |
| To Offer Her Pleasure | Ali Seay | novella | creature feature | psychological horror |
| The Tommyknockers | Stephen King | novel | aliens | science fiction |
| True Crime | Samantha Kolesnik | novella | true crime | survival horror |
| Twisted: Tainted Tales | Janine Pipe |  collection | multiple genres | short stories |
| Undertaker's Moon | Ronald Kelly | novel | werewolves | small town |
| Unfortunate Elements of my Anatomy | Hailey Piper | collection | multiple genres | short stories |
| [Waif](https://amzn.to/3RnVkTE) | Samantha Kolesnik | novella | body horror | domestic horror |
| We Have Always Lived in the Castle | Shirley Jackson | novella | family horror | psychological horror |
| [Wedding Day Massacre](https://amzn.to/3uqRuzk) | Aron Beauregard | novella | extreme horror | comedy |
| [With Teeth](https://amzn.to/3ApZ4Ob) | Brian Keene | novella | vampires | extreme horror |
| [The Worm and His Kings](https://amzn.to/3bShvkD) | Hailey Piper | novella | cosmic horror | psychological horror |
| [Yellow](https://amzn.to/3OSLeIQ) | Aron Beauregard |  novella | extreme horror | splatterpunk |
| [You've Lost a Lot of Blood](https://amzn.to/3bR4Iia) | Eric LaRocca | novella | steampunk | splatterpunk |

# What is this list?
An ongoing, sortable list of good horror books, because sometimes you just want a list of scary books or an idea of what book to read next! Simply put, these are all of the books that I've read that I enjoyed enough to recommend on this list. To start, I just grabbed all of the books that I rated 4 or higher on Goodreads. The result is a curated list of horror / crime / thriller books that I think are very good!

You can sort the list by author, book type, and keywords that represent two traits/descriptive words I chose for the book. So if you like vampires, or prefer a novella over a longer read, you can group those books together. <strong>Book titles contain Amazon affiliate links. </strong>

# A note on repeated and well-known authors
It's probably clear from this list I like Stephen King. As well-known much of his work is, sometimes you are just looking for a good new Stephen King book to read. At least I know I am! That said, like the other authors on the list, my choices here are what I consider to be King's best. There's lots of other repeats as well -- Kristopher Triana, Ania Ahlborn, Hailey Piper and others have multiple spots on my list. I like what I like! I am always looking for new authors to read. If you have any suggestions, please send me a DM on [Twitter.](https://www.twitter.com/readerofhorror)

My goal is to read as much indie horror as possible and introduce these authors to readers looking for something new. But if I wind up really liking a famous book, it's going to make the list. Maybe you haven't read it? Or maybe I just wanted to see "Dracula" and "The Shining" on another list of Must Read Horror Books.


